#!/usr/bin/env bash

whoami

npmdir="${HOME}/.npm-packages"
mkdir -p ${npmdir}
npm config set prefix $npmdir
me=`whoami`
chown -R $me $npmdir

envfix='
export NPM_PACKAGES="%s"
export NODE_PATH="$NPM_PACKAGES/lib/node_modules${NODE_PATH:+:$NODE_PATH}"
export PATH="$NPM_PACKAGES/bin:$PATH"
# Unset manpath so we can inherit from /etc/manpath via the `manpath`
# command
unset MANPATH  # delete if you already modified MANPATH elsewhere in your config
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
'
printf "${envfix}" ${npmdir} >> ~/.bashrc
source ~/.bashrc

npm install bower -g --silent
npm install gulp -g --silent
source ~/.bashrc

